# Using UltraViolet's API

This repository houses a helper Jupyter Notebook to walk colleagues through making requests to our InvenioRDM repository, UltraViolet.

## Dependencies

You need to install [jupyter notebooks](https://jupyter.org/install). You will also need to make sure you have the following Python packages installed in your development environment:

* Faker (I use 14.1.0)
* requests (I use 2.27.1)

All other required packages come with Python itself (and are declared in the notebook at the top), so you don't need to install those separately.
